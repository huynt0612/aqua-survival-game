using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    private float horizontal;
    private float speed = 8f;
    private float jumpingPower = 16f;
    private bool isFacingRight = true;
    Animator animator;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    // Start is called before the first frame update
    private void Awake()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.A))
        {
            animator.SetBool("isJump", true);
            animator.SetBool("isIdle", false);
        
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
        }
        //if (Input.GetKeyDown(KeyCode.J) || Input.GetKey(KeyCode.V))
        //{
        //    animator.SetBool("isHurt", true);
        //    animator.SetBool("isIdle", false);
        //}

        AddAttackAnimation();
        if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

       
        Flip();
    }

    public void AddAttackAnimation()
    {
        // Punch
        if (Input.GetKeyDown(KeyCode.J))
        {
            animator.SetBool("isPunch", true);
            animator.SetBool("isIdle", false);

        }else
        {
            animator.SetBool("isPunch", false);
            animator.SetBool("isIdle", true);
        }

        // Cut
        if (Input.GetKey(KeyCode.K))
        {
            animator.SetBool("isCut", true);
            animator.SetBool("isIdle", false);
        }
    }

    private void FixedUpdate()
    {
        if(horizontal==-1 || horizontal==1) animator.SetBool("isRun", true);
        if (horizontal == 0)
        {
            animator.SetBool("isRun", false);
            animator.SetBool("isIdle", true);
        }
            rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private bool IsGrounded()
    {
        Debug.Log("IsGrounded");
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            Debug.Log("flip");
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("GrassGround"))
        {
            animator.SetBool("isIdle", true);
            animator.SetBool("isJump", false);
        }
    }
}
